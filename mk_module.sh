'#!/bin/bash

## Magisk Modules
## ipdev @ xda-developers

# Set main variables

DATE=$(date '+%Y%m%d')
TDIR=$(pwd)
OUT="$TDIR"/out/"$DATE"


# Set module variables

NAME=ax562_tweeks
VER=1

# Set additional variables

ZIPNAME="$NAME"-v"$VER".zip


# Lets go

[[ ! -d $OUT ]] && mkdir -p $OUT;

# zip -r "$ZIPNAME" META-INF/* system/* base.apk customize.sh module.prop post-fs-data.sh service.sh
zip -r "$ZIPNAME" META-INF/* system/* vendor/* customize.sh module.prop post-fs-data.sh service.sh

# Finish script.
echo ""; echo "Done."; echo "";
exit 0;
