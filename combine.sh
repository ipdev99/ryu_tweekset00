####______ Script  ./service.d/00SuperSaiyanGodScripts1

#!/system/bin/sh

echo "0,1,2,4,6,15" > /sys/module/lowmemorykiller/parameters/adj

 
echo "1536,2048,4096,8192,16384,24576" > /sys/module/lowmemorykiller/parameters/minfree


echo "250" > /proc/sys/vm/dirty_expire_centisecs

echo "500" > /proc/sys/vm/dirty_writeback_centisecs



echo "30" > /proc/sys/vm/dirty_ratio


echo "3" > /proc/sys/vm/dirty_background_ratio


####______ Script  ./service.d/00SuperSaiyanGodScripts10

#kernel tweakz
echo 1 > /proc/sys/kernel/panic_on_oops;
echo 60 > /proc/sys/kernel/panic;
echo 0 > /proc/sys/kernel/panic_on_oops;
echo 30 > /proc/sys/fs/lease-break-time;
echo 64000 > /proc/sys/kernel/msgmni; #1024
echo 64000 > /proc/sys/kernel/msgmax;
echo 1000000 > /proc/sys/kernel/sched_rt_period_us;
echo 950000 > /proc/sys/kernel/sched_rt_runtime_us;
echo 500 512000 64 2048 > /proc/sys/kernel/sem;

echo 200000 > /proc/sys/kernel/sched_min_granularity_ns;


echo 400000 > /proc/sys/kernel/sched_latency_ns;


echo 100000 > /proc/sys/kernel/sched_wakeup_granularity_ns;


####______ Script  ./service.d/00SuperSaiyanGodScripts4

#!/system/bin/sh
sysctl -w vm.oom_kill_allocating_task=1
sysctl -w sysctl -w vm.page-cluster=6

####______ Script  ./service.d/00SuperSaiyanGodScripts8

#SD fix 

READ_AHEAD_KB="8192"
READ_AHEAD_KB2="4096"
READ_AHEAD_KB3="2048" 





echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:0/read_ahead_kb


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:1/read_ahead_kb


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:2/read_ahead_kb


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:3/read_ahead_kb


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:4/read_ahead_kb
    


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:5/read_ahead_kb



echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:6/read_ahead_kb


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/7:7/read_ahead_kb


echo $READ_AHEAD_KB2 > /sys/devices/virtual/bdi/default/read_ahead_kb
####______ Script  ./service.d/01edt_tweaks

#!/system/bin/sh
#
# file: /system/etc/init.d/S98edt_tweaks
#
# Created by Einherjar Development Team
#


# Remount all partitions with noatime
for k in $(busybox mount | grep relatime | cut -d " " -f3) ; do
	sync
	busybox mount -o remount,noatime $k
done

# One-time tweaks to apply on every boot
STL=`ls -d /sys/block/stl*`
BML=`ls -d /sys/block/bml*`
MMC=`ls -d /sys/block/mmc*`

# Tweak deadline io scheduler
for i in $STL $BML $MMC $TFSR; do
	echo 0 > $i/queue/rotational
	echo 1 > $i/queue/iosched/low_latency
	echo 1 > $i/queue/iosched/back_seek_penalty
	echo 1000000000 > $i/queue/iosched/back_seek_max
	echo 3 > $i/queue/iosched/slice_idle
	echo 16 > $i/queue/iosched/quantum
	echo 1 > $i/queue/iosched/fifo_batch
	echo deadline > $i/queue/scheduler
done

busybox mount -o remount,rw -t rootfs /
sh /system/bin/userinit.sh

# End EDT Tweaks - For Now
####______ Script  ./service.d/02Logger

#!/system/bin/sh

# disable logger
rm /dev/log/main

####______ Script  ./service.d/03optiflags

#!/system/bin/sh

LOOP=`ls -d /sys/block/loop*`;
RAM=`ls -d /sys/block/ram*`;
MMC=`ls -d /sys/block/mmc*`;
for j in $LOOP $RAM
do
echo "0" 
done
####______ Script  ./service.d/04journalism

#!/system/bin/sh

tune2fs -o journal_data_writeback /block/path/to/system
tune2fs -O ^has_journal /block/path/to/system
tune2fs -o journal_data_writeback /block/path/to/cache
tune2fs -O ^has_journal /block/path/to/cache
tune2fs -o journal_data_writeback /block/path/to/data
tune2fs -O ^has_journal /block/path/to/data
####______ Script  ./service.d/05netspeed

#!/system/bin/sh

echo "0" > /proc/sys/net/ipv4/tcp_timestamps;
echo "1" > /proc/sys/net/ipv4/tcp_tw_reuse;
echo "1" > /proc/sys/net/ipv4/tcp_sack;
echo "1" > /proc/sys/net/ipv4/tcp_tw_recycle;
echo "1" > /proc/sys/net/ipv4/tcp_window_scaling;
echo "5" > /proc/sys/net/ipv4/tcp_keepalive_probes;
echo "30" > /proc/sys/net/ipv4/tcp_keepalive_intvl;
echo "30" > /proc/sys/net/ipv4/tcp_fin_timeout;
echo "404480" > /proc/sys/net/core/wmem_max;
echo "404480" > /proc/sys/net/core/rmem_max;
echo "256960" > /proc/sys/net/core/rmem_default;
echo "256960" > /proc/sys/net/core/wmem_default;
echo "4096,16384,404480" > /proc/sys/net/ipv4/tcp_wmem;
echo "4096,87380,404480" > /proc/sys/net/ipv4/tcp_rmem; 


####______ Script  ./service.d/06loopy_smoothness_tweak

#!/system/bin/sh
#
# Loopy Smoothness Tweak for Galaxy S ver. Test 2 (Experimental)
#

for n in 1 2
do

  USER_LAUNCHER=com.process.acore	# Change this to your launcher app

  NUMBER_OF_CHECKS=0					# Total number of rechecks before ending 1st loop
  SLEEP_TIME=0						# Number of seconds between rechecking processes
  PROCESSES_TOTAL=14					# Must be edited to match the number of processes to be checked
  DEBUG_ECHO=0						# Debug on/off

  CHECK_COUNT=0						# Don't edit
  P_CHECK=0						# Don't edit
  CHECK_OK=0						# Unused

  PROCESS_1=0; PROCESS_2=0; PROCESS_3=0; PROCESS_4=0; PROCESS_5=0; PROCESS_6=0;
  PROCESS_7=0; PROCESS_8=0; PROCESS_9=0; PROCESS_10=0; PROCESS_11=0; PROCESS_12=0;
  PROCESS_13=0; PROCESS_14=0; PROCESS_15=0; PROCESS_16=0; PROCESS_17=0; PROCESS_18=0;
  PROCESS_19=0; PROCESS_20=0; PROCESS_21=0; PROCESS_22=0; PROCESS_23=0; PROCESS_24=0;

  if [ $n -eq "1" ]; then
    if [ $DEBUG_ECHO -eq "1" ]; then
      echo ""
      echo "LST Debug: $(date)"
      echo "LST Debug: Initiate"
    fi;

    # Pause and then loop until kswapd0 is found, which should be instant anyway
    sleep 1
    SWAP_SLEEP_TIME=3; SWAP_NUMBER_OF_CHECKS=30; SWAP_CHECK_COUNT=0; SWAP_CHECK_COUNT_OK=0; while [ $SWAP_CHECK_COUNT -lt $SWAP_NUMBER_OF_CHECKS ]; do if [ `pidof kswapd0` ]; then renice 19 `pidof kswapd0`; SWAP_CHECK_COUNT=$SWAP_NUMBER_OF_CHECKS; SWAP_CHECK_COUNT_OK=1; else sleep $SWAP_SLEEP_TIME; fi; SWAP_CHECK_COUNT=`expr $SWAP_CHECK_COUNT + 1`; done; if [ $SWAP_CHECK_COUNT_OK -lt 1 ]; then echo "LST Debug: 'kswapd0' expired after `expr $SWAP_CHECK_COUNT \* $SWAP_SLEEP_TIME` seconds"; fi;

    if [ $DEBUG_ECHO -eq "1" ]; then
      echo "LST Debug: $(date)";
      echo "LST Debug: kswapd0 found";
    fi;
  fi;

  # Check briefly one more time
  if [ $n -eq "2" ]; then
    if [ $DEBUG_ECHO -eq "1" ]; then
      echo "LST Debug: 2nd loop"
    fi;
    NUMBER_OF_CHECKS=6					# Editing not recommended
    SLEEP_TIME=5					# Editing not recommended
  fi;

  while [ $CHECK_COUNT -lt $NUMBER_OF_CHECKS ];
  do
    # Resident system apps
    if [ $PROCESS_1 -eq "0" ]; then PNAME="com.android.phone"; NICELEVEL=-20; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_1=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_2 -eq "0" ]; then PNAME="com.android.systemui"; NICELEVEL=-18; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_2=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_3 -eq "0" ]; then PNAME="com.android.settings"; NICELEVEL=-18; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_3=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_4 -eq "0" ]; then PNAME="com.swype.android.inputmethod"; NICELEVEL=-18; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_4=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_5 -eq "0" ]; then PNAME="$USER_LAUNCHER"; NICELEVEL=-18; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_5=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_6 -eq "0" ]; then PNAME="com.android.vending"; NICELEVEL=-18; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_6=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_7 -eq "0" ]; then PNAME="com.android.mms"; NICELEVEL=-19; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_7=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_8 -eq "0" ]; then PNAME="android.process.acore"; NICELEVEL=-3; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_8=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_9 -eq "0" ]; then PNAME="android.process.media"; NICELEVEL=-3; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_9=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;

    # Other system apps
    if [ $PROCESS_10 -eq "0" ]; then PNAME=""; NICELEVEL=-19; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_10=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_11 -eq "0" ]; then PNAME=""; NICELEVEL=-19; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_11=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_12 -eq "0" ]; then PNAME=""; NICELEVEL=-18; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_12=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
    if [ $PROCESS_13 -eq "0" ]; then PNAME=""; NICELEVEL=19; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_13=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;

    # org.adwfreak.launcher
    # org.zeam.core

    # Other apps
    if [ $PROCESS_14 -eq "0" ]; then PNAME="com.speedsoftware.rootexplorer"; NICELEVEL=-15; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_14=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
#    if [ $PROCESS_15 -eq "0" ]; then PNAME=""; NICELEVEL=-15; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_15=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
#    if [ $PROCESS_16 -eq "0" ]; then PNAME=""; NICELEVEL=-15; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_16=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
#    if [ $PROCESS_17 -eq "0" ]; then PNAME=""; NICELEVEL=-15; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_17=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;
#    if [ $PROCESS_18 -eq "0" ]; then PNAME=""; NICELEVEL=-15; if [ `pidof $PNAME` ]; then renice $NICELEVEL `pidof $PNAME`; PROCESS_18=1; P_CHECK=`expr $P_CHECK + 1`; fi; fi;

    # If all processes are done, loop can finish early
    if [ $P_CHECK -ge $PROCESSES_TOTAL ]; then CHECK_COUNT=$NUMBER_OF_CHECKS; else sleep $SLEEP_TIME; fi;

    CHECK_COUNT=`expr $CHECK_COUNT + 1`;
  done

  if [ $DEBUG_ECHO -eq "1" ]; then
    echo "LST Debug: $(date)"
    if [ $PROCESS_1 -eq "0" ]; then echo "LST Debug: PROCESS_1 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_2 -eq "0" ]; then echo "LST Debug: PROCESS_2 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_3 -eq "0" ]; then echo "LST Debug: PROCESS_3 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_4 -eq "0" ]; then echo "LST Debug: PROCESS_4 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_5 -eq "0" ]; then echo "LST Debug: PROCESS_5 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_6 -eq "0" ]; then echo "LST Debug: PROCESS_6 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_7 -eq "0" ]; then echo "LST Debug: PROCESS_7 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_8 -eq "0" ]; then echo "LST Debug: PROCESS_8 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_9 -eq "0" ]; then echo "LST Debug: PROCESS_9 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_10 -eq "0" ]; then echo "LST Debug: PROCESS_10 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_11 -eq "0" ]; then echo "LST Debug: PROCESS_11 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_12 -eq "0" ]; then echo "LST Debug: PROCESS_12 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_13 -eq "0" ]; then echo "LST Debug: PROCESS_13 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
    if [ $PROCESS_14 -eq "0" ]; then echo "LST Debug: PROCESS_14 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
#    if [ $PROCESS_15 -eq "0" ]; then echo "LST Debug: PROCESS_15 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
#    if [ $PROCESS_16 -eq "0" ]; then echo "LST Debug: PROCESS_16 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
#    if [ $PROCESS_17 -eq "0" ]; then echo "LST Debug: PROCESS_17 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;
#    if [ $PROCESS_18 -eq "0" ]; then echo "LST Debug: PROCESS_18 expired after `expr $CHECK_COUNT \* $SLEEP_TIME` seconds"; fi;

    echo "LST Debug: Checking complete"
  fi;

done

if [ $DEBUG_ECHO -eq "0" ]; then
  echo "LST Debug: Done"
  echo ""
fi;

####______ Script  ./service.d/07tweaks

#!/system/bin/sh
echo "0" > /proc/sys/vm/swappiness;

echo "3" > /proc/sys/vm/page-cluster;

echo "10" > /proc/sys/vm/vfs_cache_pressure; 

echo "2000" > /proc/sys/vm/dirty_writeback_centisecs;

echo "1000" > /proc/sys/vm/dirty_expire_centisecs; 

echo "0" > /proc/sys/vm/laptop_mode; 
echo "90" > /proc/sys/vm/dirty_ratio; 
echo "85" > /proc/sys/vm/dirty_background_ratio;  

echo "0" > /proc/sys/vm/oom_kill_allocating_task;  
echo "8" > /proc/sys/vm/page-cluster; 
echo "4096" > /proc/sys/vm/vm.min_free_kbytes; 
echo "10" > /proc/sys/fs/lease-break-time;
echo "0" > /proc/sys/vm/panic_on_oom; 
echo "64000" > /proc/sys/kernel/msgmni;
echo "64000" > /proc/sys/kernel/msgmax;
echo "10" > /proc/sys/fs/lease-break-time;

####______ Script  ./service.d/08darky_zipalign

#!/system/bin/sh
# DarkyROM 2011
# Much faster zipalign.

# Changelog:
# 1.2 (17/1/11) Added /data/zipalign.db file for faster apk check (ninpo,Bo$s)
# 1.1 (12/1/09) Switched to zipalign -c 4 to check the apk instead of MD5 (oknowton)
# 1.0 (11/30/09) Original

LOG_FILE=/data/zipalign.log
ZIPALIGNDB=/data/zipalign.db

if [ -e $LOG_FILE ]; then
	rm $LOG_FILE;
fi;

if [ ! -f $ZIPALIGNDB ]; then
	touch $ZIPALIGNDB;
fi;

echo "Starting FV Automatic ZipAlign $( date +"%m-%d-%Y %H:%M:%S" )" | tee -a $LOG_FILE

for DIR in /system/app /data/app ; do
  cd $DIR
  for APK in *.apk ; do
    if [ $APK -ot $ZIPALIGNDB ] && [ $(grep "$DIR/$APK" $ZIPALIGNDB|wc -l) -gt 0 ] ; then
      echo "Already checked: $DIR/$APK" | tee -a $LOG_FILE
    else
      zipalign -c 4 $APK
      if [ $? -eq 0 ] ; then
        echo "Already aligned: $DIR/$APK" | tee -a $LOG_FILE
        grep "$DIR/$APK" $ZIPALIGNDB > /dev/null || echo $DIR/$APK >> $ZIPALIGNDB
      else
        echo "Now aligning: $DIR/$APK" | tee -a $LOG_FILE
        zipalign -f 4 $APK /cache/$APK
        busybox mount -o rw,remount /system
        cp -f -p /cache/$APK $APK
        busybox rm -f /cache/$APK
        grep "$DIR/$APK" $ZIPALIGNDB > /dev/null || echo $DIR/$APK >> $ZIPALIGNDB
      fi
    fi
  done
done

busybox mount -o ro,remount /system
touch $ZIPALIGNDB
echo "Automatic ZipAlign finished at $( date +"%m-%d-%Y %H:%M:%S" )" | tee -a $LOG_FILE

####______ Script  ./service.d/09setrenice

#!/system/bin/sh
renice -20 `pidof com.android.phone`
renice -19 `pidof com.android.inputmethod.latin`
renice -19 `pidof com.swype.android.inputmethod`
renice -17 `pidof com.android.systemui`
renice -9 `pidof com.android.settings`
renice -9 `pidof com.android.vending`
renice -6 `pidof com.sec.android.app.camera`
renice -6 `pidof com.sec.android.app.fm`
renice -6 `pidof com.google.android.apps.maps`
renice -4 `pidof com.google.android.apps.googlevoice`
renice -3 `pidof android.process.media`

####______ Script  ./service.d/10lwp

renice -15 `pgrep android.process.acore`
####______ Script  ./service.d/11CFSK

#!/system/bin/sh
sync;
sysctl -w vm.drop_caches=3;
#echo 3 > /proc/sys/vm/drop_caches;
sleep 1;
sysctl -w vm.drop_caches=0;
#echo 0 > /proc/sys/vm/drop_caches;
echo "Caches are dropped!";

####______ Script  ./service.d/12cleaner

#!/system/bin/sh
#
# Copyright (c) 2011 Leonard Luangga


#remove cache, tmp, and unused files
rm -f /cache/*.apk
rm -f /cache/*.tmp
rm -f /data/dalvik-cache/*.apk
rm -f /data/dalvik-cache/*.tmp

if [ -e /data/system/userbehavior.db ]
then
  rm -f /data/system/userbehavior.db
fi

if [ -d /data/system/usagestats ]
then
  chmod 400 /data/system/usagestats
fi

if [ -d /data/system/appusagestats ]
then
  chmod 400 /data/system/appusagestats
fi


#remove main log
if [ -e /dev/log/main ]
then
  rm -f /dev/log/main
fi


####______ Script  ./service.d/13citbun

#!/system/bih/sh
#
# Copyright (c) 2011 Leonard Luangga


#internet speed tweaks
echo 0 > /proc/sys/net/ipv4/tcp_timestamps;
echo 0 > /proc/sys/net/ipv4/tcp_tw_recycle;
echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse;
echo 1 > /proc/sys/net/ipv4/tcp_sack;
echo 1 > /proc/sys/net/ipv4/tcp_window_scaling;
echo 5 > /proc/sys/net/ipv4/tcp_keepalive_probes;
echo 20 > /proc/sys/net/ipv4/tcp_keepalive_intvl;
echo 1800 > /proc/sys/net/ipv4/tcp_keepalive_time;
echo 30 > /proc/sys/net/ipv4/tcp_fin_timeout;
echo 404480 > /proc/sys/net/core/wmem_max;
echo 404480 > /proc/sys/net/core/rmem_max;
echo 256960 > /proc/sys/net/core/rmem_default;
echo 256960 > /proc/sys/net/core/wmem_default;
echo 4096 16384 404480 > /proc/sys/net/ipv4/tcp_wmem;
echo 4096 87380 404480 > /proc/sys/net/ipv4/tcp_rmem;


#battery tweaks (sleepers)
#mount -t debugfs none /sys/kernel/debug
#echo NO_NEW_FAIR_SLEEPERS > /sys/kernel/debug/sched_features;
#echo NO_NORMALIZED_SLEEPERS > /sys/kernel/debug/sched_features;
#umount /sys/kernel/debug

#if [ -e /proc/sys/kernel/sched_min_granularity_ns ]
#then
#echo 200000 > /proc/sys/kernel/sched_min_granularity_ns;
#fi

#if [ -e /proc/sys/kernel/sched_latency_ns ]
#then
#echo 400000 > /proc/sys/kernel/sched_latency_ns;
#fi

#if [ -e /proc/sys/kernel/sched_wakeup_granularity_ns ]
#then
#echo 100000 > /proc/sys/kernel/sched_wakeup_granularity_ns;
#fi

#if [ -e /proc/sys/kernel/hung_task_timeout_secs ]
#then
#echo 45 > /proc/sys/kernel/hung_task_timeout_secs;s
#(busybox expr `cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_transition_latency` \* 750 / 1000);
#echo $SAMPLING_RATE > /sys/devices/system/cpu/cpu0/cpufreq/ondemand/sampling_rate;
#echo 10000 > /sys/devices/system/cpu/cpu0/cpufreq/ondemand/sampling_rate;
#echo 70 > /sys/devices/system/cpu/cpu0/cpufreq/ondemand/up_threshold;

####______ Script  ./service.d/14enable_touchscreen_1

#!/system/bin/sh
#Touchscreen (Slaid480)
#Configure touchscreen sensitivity
#Sensitive(Chainfire)

echo 7035 > /sys/class/touch/switch/set_touchscreen;
echo 8002 > /sys/class/touch/switch/set_touchscreen;
echo 11000 > /sys/class/touch/switch/set_touchscreen;
echo 13060 > /sys/class/touch/switch/set_touchscreen;
echo 14005 > /sys/class/touch/switch/set_touchscreen;

####______ Script  ./service.d/15cleanup_init_ram

#!/bin/sh

/bin/mount -o remount,rw /

/bin/rm -r /voodoo/root/usr/*

/bin/mount -o remount,ro /


####______ Script  ./service.d/16sqlite_optimize

#!/system/bin/sh

# ==============================================================
# Optimize SQlite databases of apps
# ==============================================================

echo "";
echo "*********************************************";
echo "Optimizing and defragging your database files (*.db)";
echo "Ignore the 'database disk image is malformed' error";
echo "Ignore the 'no such collation sequence' error";
echo "*********************************************";
echo "";

for i in \
`busybox find /data -iname "*.db"`; 
do \
	/system/xbin/sqlite3 $i 'VACUUM;'; 
	/system/xbin/sqlite3 $i 'REINDEX;'; 
done;

if [ -d "/dbdata" ]; then
	for i in \
	`busybox find /dbdata -iname "*.db"`; 
	do \
		/system/xbin/sqlite3 $i 'VACUUM;'; 
		/system/xbin/sqlite3 $i 'REINDEX;'; 
	done;
fi;


if [ -d "/datadata" ]; then
	for i in \
	`busybox find /datadata -iname "*.db"`; 
	do \
		/system/xbin/sqlite3 $i 'VACUUM;'; 
		/system/xbin/sqlite3 $i 'REINDEX;'; 
	done;
fi;


for i in \
`busybox find /sdcard -iname "*.db"`; 
do \
	/system/xbin/sqlite3 $i 'VACUUM;'; 
	/system/xbin/sqlite3 $i 'REINDEX;'; 
done;

####______ Script  ./service.d/17speedy_modified

#!/system/bin/sh
#
# 
#
rm -r /data/local/tmp/*
rm -r /data/tmp/*
rm -r /data/system/usagestats/*
rm -r /data/system/appusagestats/*
rm -r /data/system/dropbox/*
rm -r /data/tombstones/*
rm -r /data/anr/*
busybox chmod 000 /data/system/userbehavior.db
busybox chmod 000 /data/system/usagestats/
busybox chmod 000 /data/system/dropbox/
busybox chmod 000 /data/anr/
busybox chmod 000 /data/tombstones/
busybox chmod 000 /data/system/appusagestats/
busybox chmod 000 /data/data/com.google.android.location/files/cache.cell
busybox chmod 000 /data/data/com.google.android.location/files/cache.wifi

# Allow configuring of both cores
#echo "1" > /sys/devices/system/cpu/cpu0/online;
#echo "1" > /sys/devices/system/cpu/cpu1/online;

# Remount each file system with noatime and nodiratime flags to save battery and CPU cycles
# and change journaling mode for data and cache
#
busybox mount -o remount,noatime,nodiratime,remount,rw,barrier=0,data=writeback /data;
busybox mount -o remount,noatime,nodiratime,remount,rw,barrier=0,data=writeback /cache;
busybox mount -o remount,noatime,nodiratime /dev;
busybox mount -o remount,noatime,nodiratime /proc;
busybox mount -o remount,noatime,nodiratime /sys;
busybox mount -o remount,noatime,nodiratime /system;


# Flag 
# --------------
#
# 
#.
#

MMC=`ls -d /sys/block/mmc*`;
for q in $MMC
do
#	echo "deadline" > $q/queue/scheduler;
#	echo "512" > $q/queue/read_ahead_kb;
	echo "0" > $q/queue/rotational;
	echo "1" > $q/queue/iosched/fifo_batch;
	echo "1" > $q/queue/iosched/low_latency;
	echo "1" > $q/queue/iosched/back_seek_penalty;
	echo "1000000000" > $q/queue/iosched/back_seek_max;
	echo "3" > $q/queue/iosched/slice_idle; 
	echo "16" > $q/queue/iosched/quantum
	echo "512" > $q/queue/nr_requests;
done

echo "2048" > /sys/devices/virtual/bdi/179:0/read_ahead_kb; # Set sd card read ahead cache size

#
# END SECTION--------------


# /proc/sys/net/ipv4
sysctl -w net.ipv4.tcp_timestamps=0;
sysctl -w net.ipv4.tcp_tw_reuse=1;
sysctl -w net.ipv4.tcp_tw_recycle=1;
sysctl -w net.ipv4.tcp_sack=1;
sysctl -w net.ipv4.tcp_window_scaling=1;
sysctl -w net.ipv4.tcp_keepalive_probes=5; # Retries before connection is considered dead
sysctl -w net.ipv4.tcp_keepalive_intvl=156; # 
sysctl -w net.ipv4.tcp_fin_timeout=30;
sysctl -w net.ipv4.tcp_rmem='6144	87380 524288'; # 65536	131072 524288
sysctl -w net.ipv4.tcp_wmem='6144	131072 524288'; # 65536	131072 524288
sysctl -w net.ipv4.tcp_ecn=0; # Explict Congestion Notification
sysctl -w net.ipv4.tcp_max_tw_buckets=360000; # 
sysctl -w net.ipv4.tcp_synack_retries=2; #Default: 5

# Ignore all icmp packets or pings
sysctl -w net.ipv4.icmp_echo_ignore_all=1;
sysctl -w net.ipv6.icmp_echo_ignore_all=1;

# Net Core Settings
# Location: /proc/sys/net/core
sysctl -w net.core.wmem_max=524288; #404480
sysctl -w net.core.rmem_max=524288; #404480
sysctl -w net.core.rmem_default=110592;
sysctl -w net.core.wmem_default=110592;

#
sysctl -w net.ipv4.route.flush=1;


# Kernel Tweaks
#
sysctl -w kernel.sched_features=15834233;
#sysctl -w kernel.msgmni=2048;
#sysctl -w kernel.msgmax=64000;
#sysctl -w kernel.shmmax=268435500;
#sysctl -w kernel.sem=500,512000,64,2048;
#sysctl -w kernel.hung_task_timeout_secs=0;
#sysctl -w kernel.sched_latency_ns=5000000; #600000
#sysctl -w kernel.sched_compat_yield=1;
#sysctl -w kernel.sched_shares_ratelimit=256000;
#sysctl -w kernel.sched_child_runs_first=0;
#sysctl -w kernel.threads-max=10000;

####______ Script  ./service.d/18complete

#!/system/bin/sh

sync;
setprop cm.filesystem.ready 1;

mount -o remount rw /
mount -o remount rw /system
####______ Script  ./service.d/19build


######________ Move to system.prop file ______#########
# #!/system/bin/sh
# #
# #st@matis build.prop tweak script
# #
# # build.prop tweaks
# setprop dalvik.vm.execution-mode=int:jit
# setprop persist.sys.purgeable_assets=1
# setprop dalvik.vm.dexopt-flags=m=y
# setprop ro.media.enc.jpeg.quality=90
# setprop dalvik.vm.heapsize=48m
# setprop ro.telephony.call_ring.delay=0
# setprop debug.performance.tuning=1
# setprop ro.ril.disable.power.collapse=0
# setprop pm.sleep_mode=1
# setprop video.accelerate.hw=1
# setprop windowsmgr.max_events_per_sec=180
# setprop ro.ril.disable.power.collapse=1
# setprop wifi.supplicant_scan_interval=150
# setprop mot.proximity.delay=150
# setprop persist.sys.shutdown.mode=hibernate
# setprop ro.ext4fs=1
# setprop ro.config.hwfeature_wakeupkey=0
# setprop dalvik.vm.startheapsize=4
# setprop ro.kernel.android.checkjni=0
# setprop ro.ril.hsxpa=0
# setprop ro.ril.hsupa.category=6;
# setprop ro.ril.hsdpa.category=8;
# setprop ro.ril.enable.dtm=1;
# setprop ro.ril.enable.a53=1;
# setprop ro.ril.enable.3g.prefix=1;
# setprop keyguard.no_require_sim=true;
# setprop net.tcp.buffersize.default=4096,87380,256960,4096,16384,256960;
# setprop net.tcp.buffersize.wifi=4096,87380,256960,4096,16384,256960;
# setprop net.tcp.buffersize.umts=4096,87380,256960,4096,16384,256960;
# setprop net.tcp.buffersize.edge=4096,87380,256960,4096,16384,256960;
# setprop net.tcp.buffersize.gprs=4096,87380,256960,4096,16384,256960;
# setprop windowsmgr.support_rotation_270=true;
# setprop ro.mot.eri.losalert.delay=000;
# setprop ro.lge.proximity.delay=25;
# setprop ro.media.dec.jpeg.memcap=20000000;

######________ Move to service.d script file ______#########

# sysctl -w net.ipv6.conf.all.disable_ipv6=1
######_____________________________________________#########

####______ Script  ./service.d/swap

sysctl vm.swappiness=64
####______ Script  ./service.d/xfiles


####______ Script  ./service.d/zTTL

echo "65"  > /proc/sys/net/ipv4/ip_default_ttl
sysctl -w net.ipv6.conf.all.disable_ipv6=1
